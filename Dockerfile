FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1
COPY ./requiriments.txt /requirements.txt
RUN pip install -r /requirements.txt

RUN mkdir /users-web-framework
COPY ./users-web-framework /users-web-framework
WORKDIR /users-web-framework
